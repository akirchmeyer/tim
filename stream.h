#ifndef STREAM_H
#define STREAM_H

#include <stdint.h>
#include <stdlib.h>

typedef struct dblock_t {
    uint8_t *data;
    size_t count;
} dblock_t;

typedef int(*pStream)(dblock_t*, void*);

typedef struct stream_t {
    pStream pFunc;
    void* pData;
} stream_t;

#define STREAM_OK 0
#define STREAM_END 1
#define STREAM_ERROR -1

void print_hex(dblock_t *d);
int stream_execute(stream_t *stream, dblock_t *buf);

int read_data(dblock_t *d, void *data);
int write_data(dblock_t *d, void *data);

int read_file(dblock_t *d, void *file);
int write_file(dblock_t *d, void *file);


size_t getFileSize(const char *n);

typedef struct chunk_t {
    stream_t *pStream;
    size_t size;
} chunk_t;

//pChunk should be of type chunk_t*
void stream_chunk(dblock_t *d, void *pChunk);

typedef struct node_t {
    stream_t *pStream;
    struct node_t *next;
} node_t;

void stream_chain(dblock_t *d, void *pChain);

typedef struct managed_t {
    stream_t *pStream;
    uint8_t* data;
    size_t size, offset;
} managed_t;

void stream_managed(dblock_t *d, void *pManaged);

managed_t* managed_create(stream_t* stream, size_t c_size);
void managed_destroy(managed_t *m);


#endif
