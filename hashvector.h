#ifndef HASHVECTOR_H
#define HASHVECTOR_H

#include "hashmap.h"
#include "vector.h"

typedef struct h_vector {
    vector k, v;
    hashmap h;
} h_vector;

void hv_construct(h_vector *h, size_t s);
void hv_destruct(h_vector *h);

void* hv_get(h_vector *h, uint32_t k);
void* hv_get_unsafe(h_vector *h, uint32_t k);

void hv_push_header(h_vector *h, uint32_t k);

size_t hv_save(h_vector *h);
void hv_restore(h_vector *h, size_t s);

#endif
