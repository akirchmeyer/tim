#include "program.h"
#include <assert.h>

program *program_create(dblock_t *d) {
    program *p = malloc(sizeof(program));
    p->data = d->data;
    p->count = d->count;
    p->size = 0;
    return p;
}

void program_destroy(program *p) {
    free(p);
}

#ifdef OS_WINDOWS
#include <windows.h>
#error "Unimplemented"
#elif defined(OS_POSIX)

#include <sys/mman.h>
#include <unistd.h>

size_t getPageSize() {
    return (size_t)sysconf(_SC_PAGESIZE);
}

#include <stdio.h>

#ifndef MAP_ANONYMOUS
#include <fcntl.h>
#endif

void dblock_map(dblock_t *d, int prot) {
#ifdef MAP_ANONYMOUS
    const int flags = MAP_ANONYMOUS | MAP_PRIVATE;
    d->data = mmap(NULL, d->count, prot, flags, -1, 0);
#else
    int fd = open("/dev/zero", O_RDWR);
    assert(fd != -1);
    if(fd == -1) {
        fprintf(stderr, "open failed\n");
        exit(EXIT_FAILURE);
    }

    d->data = mmap(NULL, d->count, prot, MAP_PRIVATE, fd, 0);
    if (close(fd) == -1) {          /*No longer needed*/
        fprintf(stderr, "close() failed\n");
        exit(EXIT_FAILURE);
    }
#endif
    assert(d->data != MAP_FAILED);
}

void dblock_unmap(dblock_t* d) {
    munmap(d->data, d->count);
    d->data = NULL;
}

void dblock_protect(dblock_t *d, int prot) {
    mprotect(d->data, d->count, prot);
}

#endif

#include <string.h>

void program_emit_b64(program *p, uint64_t s, size_t count) {
    assert(p->size+count < p->count);
#ifdef ARCH_LE
    uint8_t *dst = p->data + p->size;
    uint8_t *src = (uint8_t*)&s + count-1;
    for(size_t i = 0; i < count; ++i, ++dst, --src)
        *dst = *src;
#else
    memcpy(p->data + p->size, &s, count);
#endif
    p->size += count;
}

void program_emit_d64(program *p, uint64_t s, size_t count) {
    program_emit(p, (uint8_t*)&s, count);
}

void program_emit(program *p, uint8_t *src, size_t count)
{
    assert(p->size + count < p->count);

    memcpy(p->data + p->size, src, count);
    p->size += count;
}
