#include "mk.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>

//generate states first
mk_process* mkCreateProcess(stream_t *rs, stream_t *ws, mk_pFunc *f, size_t *n, size_t size) {
    mk_process* p = malloc(sizeof(mk_process));
    p->rs = rs;
    p->ws = ws;
    p->func = f;
    p->next = n;
    p->size = size;

    return p;
}

void mkDestroyProcess(mk_process* p) {
    free(p);
}

// Do not forget to take care of state 0 (ie. clear the stack)
void mkProcessExecute(mk_process *p, void *data) {
    stream_t *rs = p->rs;

    assert(p->size > 0);

    mk_pFunc* funcs = p->func;
    size_t *nexts = p->next;

    mk_machine m;
    m.data = data;
    m.ws = p->ws;

    dblock_t db;
    db.count = 1;
    db.data = &m.byte;

    int err = STREAM_OK;

    mk_pFunc *state = funcs;
    while(err == STREAM_OK) {
        err = stream_execute(rs, &db);

        while(!(*state)(&m))
            ++state;

        state = funcs + nexts[state - funcs];
    }
}
