#ifndef MK_H
#define MK_H

#include "vector.h"
#include "stream.h"

// - state 0 is base state, ie. finished processing element
// - use 'stack' specific to pass to store information
// - Use several processes at once and chain units (output to streams)

struct mk_machine;
typedef int(*mk_pFunc)(struct mk_machine*);

typedef struct mk_process {
    stream_t *rs, *ws;
    mk_pFunc *func;
    size_t *next;
    size_t size;
} mk_process;

typedef struct mk_machine {
    stream_t *ws;
    void *data;
    uint8_t byte;
} mk_machine;

mk_process* mkCreateProcess(stream_t *rs, stream_t *ws, mk_pFunc *f, size_t *n, size_t size);
void mkDestroyProcess(mk_process *p);
void mkProcessExecute(mk_process *p, void *data);

#endif // MK_H
