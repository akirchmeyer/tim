#include "stream.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

int stream_execute(stream_t *stream, dblock_t *buf) {
    return stream->pFunc(buf, stream->pData);
}

size_t min(size_t a, size_t b) {
    return (a < b) ? a : b;
}

int read_data(dblock_t *d, void *data) {
    dblock_t *d2 = data;

    void* ret = memcpy(d->data, d2->data, min(d->count, d2->count));
#ifdef NDEBUG
    (void) ret;
#endif
    assert(ret != NULL);

    if(d2->count < d->count)
        memset(d->data + d2->count, '\0', d->count - d2->count);
    if(d2->count <= d->count) {
        d2->count = 0;
        return STREAM_END;
    }

    d2->count -= d->count;
    d2->data += d->count;

    return STREAM_OK;
}

int write_data(dblock_t *d, void *data) {
    dblock_t *d2 = data;
    void *ret = memcpy(d2->data, d->data, min(d2->count, d->count));
#ifdef NDEBUG
    (void) ret;
#endif
    assert(ret != NULL);

    if(d2->count <= d->count) {
        d2->count = 0;
        return STREAM_END;
    }

    d2->count -= d->count;
    d2->data += d->count;

    return STREAM_OK;
}

#include <errno.h>

int read_file(dblock_t *d, void *file) {
    const size_t n = fread(d->data, sizeof(uint8_t), d->count, file);
    assert(!ferror(file));

    if(n < d->count)
        memset(d->data + n, '\0', (d->count - n));
    if(n <= d->count)
        return STREAM_END;

    return STREAM_OK;
}

int write_file(dblock_t *d, void *file) {
    fwrite(d->data, sizeof(uint8_t), d->count, file);
    assert(!ferror(file));

    return STREAM_OK;
}

#include "arch.h"

#ifdef OS_POSIX

#include <sys/stat.h>
size_t getFileSize(const char *n) {
    struct stat st;
    stat(n, &st);
    return (size_t)st.st_size;
}

#else
#error "Unimplemented: see https://stackoverflow.com/questions/238603/how-can-i-get-a-files-size-in-c"
#endif

void stream_chunk(dblock_t *d, void *pChunk) {
    const size_t size = ((chunk_t*)pChunk)->size;
    stream_t* s = ((chunk_t*)pChunk)->pStream;

    size_t count = d->count;

    dblock_t d2;
    d2.count = size;
    d2.data = d->data;

    while(count > size){
        stream_execute(s, &d2);
        d2.data += size;
        count -= size;
    }
    if(count > 0){
        d2.count = count;
        stream_execute(s, &d2);
    }
}

void stream_chain(dblock_t *d, void *pChain){
    node_t* node = pChain;

    while(node != NULL){
        stream_execute(node->pStream, d);
        node = node->next;
    }
}

void stream_managed(dblock_t *d, void *pManaged){
    managed_t* managed = pManaged;

    uint8_t *m_data = managed->data;
    const size_t size = managed->size;

    size_t count = d->count;

    dblock_t d2;
    d2.data = d->data;
    d2.count = size;
    {
        const size_t offset = managed->offset, n = min(size - offset, count);
        if(n > 0){
            memcpy(d2.data, m_data + offset, n);
            managed->offset += n;
            d2.data += n;
            count -= n;
        }
    }

    stream_t *s = managed->pStream;

    while(count > size) {
        stream_execute(s, &d2);
        d2.data += size;
        count -= size;
    }

    if(count > 0){
        uint8_t *data2 = d2.data;
        d2.count = count;
        d2.data = m_data;
        stream_execute(s, &d2);

        memcpy(d2.data, data2, count);
        managed->offset = count;
    }
}

managed_t *managed_create(stream_t* stream, size_t c_size){
    managed_t *m = malloc(sizeof(managed_t));
    m->pStream = stream;
    m->data = malloc(c_size);
    m->size = c_size;
    m->offset = 0;
    return m;
}

void managed_destroy(managed_t *m) {
    free(m->data);
    m->data = NULL;
    free(m);
}

void print_hex(dblock_t *d)
{
    for(size_t i = 0; i < d->count; ++i)
        printf("%02X ", d->data[i]);
    printf("\n");
}
