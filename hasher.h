#ifndef HASHER_H
#define HASHER_H

#define HASHER_MOD 1000000007

#include <stdint.h>
#include <stdlib.h>

uint32_t hash(const uint32_t *H, size_t n, const uint8_t *d);
uint32_t* hasher_create();
void hasher_destroy(uint32_t *H);


#endif
