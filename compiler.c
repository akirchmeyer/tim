#include "compiler.h"
#include "hasher.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>

compiler *compiler_create(uint8_t *data) {
    compiler *c = malloc(sizeof(compiler));
    const size_t N = 1024;
    hv_construct(&c->sym, N);
    c->data = data;
    c->size = 0;
    c->H = hasher_create();
    caller_construct(&c->c);

    return c;
}

void caller_construct(caller *c) {
    v_construct(&c->v_call);
    v_construct(&c->v_args);

    const size_t N = 1024;
    hv_construct(&c->var, N);

    c->n_args = 0;
    c->i_sym = 0;
}

void caller_destruct(caller *c) {
    v_destruct(&c->v_args);
    v_destruct(&c->v_call);
    hv_destruct(&c->var);
}

void compiler_destroy(compiler *c) {
    compiler_pop_symbols(c, 0);
    hv_destruct(&c->sym);
    caller_destruct(&c->c);
    hasher_destroy(c->H);
    free(c);
}

void test() {
    printf("Hello world!\n");
}

#include "dlib.h"
#include <gnu/lib-names.h>

void stdlib_printf(compiler *c) {
    compiler_emit_instr(c, 0x48BA, 2);
    compiler_emit(c,(uint8_t*) &(uint64_t) {
        (uint64_t)test
    }, sizeof(uint64_t));

    compiler_emit_instr(c, 0xFFD2, 2); //call rdx

    dlib *dlibc = dlib_open(LIBC_SO);

    void *d_printf = dlib_loadsymbol(dlibc, "printf");
    dlib_close(dlibc);

    /*compiler_emit_instr(c, 0xE8, 1); //call rel32
    int64_t o = (int64_t)((intptr_t)test - (intptr_t)c->data - sizeof(int32_t) - c->size);
    */printf("%ld\n", (intptr_t)d_printf - (intptr_t)(c->data + c->size + sizeof(int32_t)));
    //compiler_emit(c, (uint8_t*)&o, sizeof(int32_t));
    compiler_emit_instr(c, 0xC3, 1); //ret
}

void lang_newblock(compiler *c) {
    (void) c;
}

void lang_endblock(compiler *c) {
    compiler_call(c);
}

void dbg_sym(compiler *c) {
    compiler_print_symbols(c);
}

#define CHAR_ARRAY(s) sizeof(s)-1, (uint8_t*)s

void compiler_link_std(compiler *c) {
    compiler_add_symbol(c, compiler_hash(c, CHAR_ARRAY("printf")), -1, stdlib_printf);

    compiler_add_symbol(c, compiler_hash(c, CHAR_ARRAY("{")), -1, lang_newblock);
    compiler_add_symbol(c, compiler_hash(c, CHAR_ARRAY("}")), 0, lang_endblock);
    compiler_add_symbol(c, compiler_hash(c, CHAR_ARRAY("#dbg_sym")), 0, dbg_sym);
}

void caller_print_stack(caller *c) {
    vector *v_stack = &c->v_args;
    uint8_t* stack = v_stack->data;
    size_t n = v_stack->size;
    printf("\n[Stack Info]:\n");
    while(n > 0) {
        literal_info *i = (literal_info*)stack;
        stack += sizeof(literal_info);

        switch(i->info) {
        case LITERAL_I64:
            printf("%ld ", *(int64_t*)stack);
            break;
        case LITERAL_D64:
            printf("%.9f ", *(double*)stack);
            break;
        case LITERAL_DATA:
            printf("\"%.*s\" ", (int)i->count, (const char*)stack);
            break;
        default:
            fprintf(stderr, "Error: unrecognized literal type\n");
            exit(-1);
        }
        stack += i->count;
        n -= i->count + sizeof(literal_info);
    }
    printf("\n");
    fflush(stdout);
}

void compiler_print_symbols(compiler *c)
{
    printf("\n[Symbols Info]:\n");
    hm_print(&c->sym.h);
    printf("\n");
}

void caller_print_variables(caller *c) {
    (void) c;

    /*vector *v_var = c2->v_var;
    uint8_t *d = v_sym->data;
    size_t n = v_sym->size;

    while(n > 0) {
        symbol_info *i = (symbol_info*)d;
        printf("[%u] ", i->k);
        assert(hm_get(c->h_sym, i->k));
        size_t m = sizeof(symbol_info) + i->count;
        d += m;
        n -= m;
    }

    printf("\n");*/
}

void compiler_push_i64(compiler *c, int64_t v) {
    caller *c2 = &c->c;

    literal_info i = {.info = LITERAL_I64, .count = sizeof(int64_t)};
    v_push(&c2->v_args, &i, sizeof(literal_info));
    v_push(&c2->v_args, &v, sizeof(int64_t));
    compiler_push_arg(c);
}

void compiler_push_d64(compiler *c, double v) {
    caller *c2 = &c->c;

    literal_info i = {.info = LITERAL_D64, .count = sizeof(double)};
    v_push(&c2->v_args, &i, sizeof(literal_info));
    v_push(&c2->v_args, &v, sizeof (double));
    compiler_push_arg(c);
}

void compiler_push_data(compiler *c, size_t n, uint8_t *data) {
    caller *c2 = &c->c;
    literal_info i = {.info = LITERAL_DATA, .count = n};
    v_push(&c2->v_args, &i, sizeof(literal_info));
    v_push(&c2->v_args, data, n);
    compiler_push_arg(c);
}

typedef void(*pFunc)(compiler *c);

void compiler_add_symbol(compiler *c, uint32_t k, int32_t n_args, void *d) {
    vector *v_sym = &c->sym.v;
    symbol_info i = { .n_args = n_args };
    v_push(v_sym, &i, sizeof(symbol_info));

    hv_push_header(&c->sym, k);

    v_push(v_sym, &d, sizeof(void*));
    printf("[%u]: %p\n", k, d);
}

void compiler_push_arg(compiler *c) {
    caller *c2 = &c->c;
    ++c2->n_args;
    compiler_try_call(c);
}

void compiler_try_call(compiler *c) {
    caller *c2 = &c->c;
    if(c2->v_call.size == 0)
        return;

    vector *v_sym = &c->sym.v;
    symbol_info *si = (symbol_info*)v_byte(v_sym, c2->i_sym - sizeof(symbol_info));

    vector *v_call = &c2->v_call;
    call_info *ci = v_back(v_call, call_info);
    if(si->n_args < 0 || (c2->n_args - ci->i_args) != (uint32_t)si->n_args)
        return;

    compiler_call(c);
}


void compiler_call(compiler *c) {
    caller *c2 = &c->c;
    if(c2->v_call.size == 0)
        return;

    vector *v_call = &c2->v_call;
    call_info *ci = v_back(v_call, call_info);

    //Prepare call
    vector *v_sym = &c->sym.v;
    pFunc func = *(pFunc*)v_byte(v_sym, c2->i_sym);

    //Restore
    vector *v_args = &c2->v_args;
    v_args->size = ci->i_stack;
    c2->n_args = ci->i_args;
    v_call->size -= sizeof(call_info);

    //Find next
    --ci;

    if(!v_empty(v_call))
        c2->i_sym = (size_t)hm_get_unsafe(&c->sym.h, ci->k);

    func(c);
}

void compiler_pop_symbols(compiler *c, size_t s) {
    hv_restore(&c->sym, s);
}

uint32_t compiler_hash(compiler *c, size_t n, const uint8_t *d) {
    return hash(c->H, n, d);
}

void compiler_push_call(compiler *c, uint32_t k) {
    caller *c2 = &c->c;
    call_info i = {
        .k = k,
        .i_args = c2->n_args,
        .i_stack = c2->v_args.size
    };
    v_push(&c2->v_call, &i, sizeof(call_info));

    c2->i_sym = (size_t)hm_get_unsafe(&c->sym.h, k);

    compiler_try_call(c);
}

#include "arch.h"
#include <byteswap.h>

void compiler_emit(compiler *c, const uint8_t *src, size_t n) {
    memcpy(c->data + c->size, src, n);
    c->size += n;
}

void compiler_emit_instr(compiler *c, uint64_t s, size_t n) {
#ifdef ARCH_LE
    uint8_t *dst = c->data + c->size;
    uint8_t *src = (uint8_t*)&s + n-1;
    c->size += n;
    while(n > 0)
        *dst = *src, --n, ++dst, --src;
#else
    compiler_emit(c, (const uint8_t*)&src, n);
#endif
}
