﻿#ifndef COMPILER_H
#define COMPILER_H
//TODO: Add memory allocator for symbols at least

#include <stdint.h>
#include <stdlib.h>
#include "vector.h"
#include "hashvector.h"

#define L_VAL(v, t) ((t){v})
#define L_U8(v)  L_VAL(v, uint8_t)
#define L_U32(v) L_VAL(v, uint32_t)
#define L_U64(v) L_VAL(v, uint64_t)
#define L_I32(v) L_VAL(v, int32_t)
#define L_I64(v) L_VAL(v, int64_t)
#define L_ST(v)  L_VAL(v, size_t)

#define LITERAL_I64 0
#define LITERAL_D64 1
#define LITERAL_DATA 2

typedef struct __attribute__((__packed__)) literal_info {
    size_t count;
    uint8_t info;
} literal_info;

typedef struct __attribute__((__packed__)) symbol_info {
    int32_t n_args;
} symbol_info;

typedef struct __attribute__((__packed__)) call_info {
    uint32_t k;
    size_t i_stack, i_args;
} call_info;

typedef struct caller {
    vector v_call, /**v_iargs, */v_args;
    h_vector var;
    size_t n_args, i_sym;
} caller;

void caller_construct(caller *c);
void caller_destruct(caller *c);

#include "stream.h"

typedef struct compiler {
    caller c;
    h_vector sym;
    uint8_t *data;
    size_t size;
    uint32_t *H;
} compiler;

compiler *compiler_create(uint8_t *data);
void compiler_destroy(compiler *c);

//Symbols
void compiler_add_symbol(compiler *c, uint32_t k, int32_t n_args, void *d);
void compiler_link_std(compiler *c);
void compiler_push_call(compiler *c, uint32_t k);
void compiler_push_arg(compiler *c);
void compiler_try_call(compiler *c);
void compiler_call(compiler *c);

void compiler_emit(compiler *c, const uint8_t *src, size_t n);
void compiler_emit_instr(compiler *c, uint64_t s, size_t n);

//Literals
void compiler_push_i64(compiler *c, int64_t v);
void compiler_push_d64(compiler *c, double v);
void compiler_push_data(compiler *c, size_t n, uint8_t *d);

uint32_t compiler_hash(compiler *c, size_t n, const uint8_t *d);

//Stack
void compiler_stack_call(compiler *c);

//Debug
void caller_print_stack(caller *c);
void caller_print_variables(caller *c);
void compiler_print_symbols(compiler *c);

void compiler_pop_symbols(compiler *c, size_t s);

#endif
