#ifndef VECTOR_H
#define VECTOR_H

#include <stdlib.h>

typedef struct vector {
    void *data;
    size_t size, count;
} vector;

#define A_SIZE 128

vector* v_create();
void v_construct(vector *v);
void v_destruct(vector *v);
void v_destroy(vector *v);

void v_reserve(vector *v, const size_t s);

void v_push(vector *v, const void *b, const size_t size);
void v_pop(vector *v, const size_t size);
void* v_append(vector *v, const size_t size);

//#define v_data(v,t) ((t)((v)->data))
#define v_byte(v, i) ((uint8_t*)(v)->data + (i))
#define v_front(v, t) ((t)((v)->data))
#define v_back(v, t) ((t*)(v_byte((v), (v)->size - sizeof(t))))
#define v_empty(v) ((v)->size == 0)
#define v_pop(v, s) ((v)->size -= s)

#endif
