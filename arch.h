#ifndef ARCH_H
#define ARCH_H

#define ARCH_X64 1
#define ARCH_X86 2
#define ARCH_ARM 3

#if defined(_WIN32) || defined(WIN32) || defined(__WIN32)
#define OS_WINDOWS
#elif (defined(__linux__) || defined(__FreeBSD__) || defined(__FreeBSD_kernel__) || \
    defined(__NetBSD__) || defined(__OpenBSD__)) && !defined(__ORBIS__)
#define OS_POSIX

#else
#error "Unimplemented"
#endif

#if defined(__x86_64__) || defined(__x86_64) || defined(_M_X64) || defined(_M_AMD64)
#define TARGET_ARCH ARCH_X64
#define ARCH_LE

#else
#error "Unimplemented"
#endif

#if !defined(ARCH_LE) && !defined(ARCH_BE)
#error "Error: endianness not defined"
#endif

#endif
