#include "vector.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>

vector* v_create() {
    vector *v = malloc(sizeof(vector));
    v_construct(v);
    return v;
}
void v_destroy(vector* v) {
    v_destruct(v);
    free(v);
}

void v_construct(vector *v) {
    v->data = NULL;
    v->size = 0;
    v->count = 0;
}

void v_destruct(vector *v) {
    if(v->data != NULL) {
        free(v->data);
        v->data = NULL;
        v->count = 0;
        v->size = 0;
    }
}

void v_reserve(vector *v, const size_t s) {
    size_t *count = &v->count;
    if(*count < s) {
        if(*count == 0)
            *count = A_SIZE;
        while(*count < s)
            *count <<= 1;
        v->data = realloc(v->data, *count);
        assert(v->data != NULL);
    }
}

void v_push(vector* v, const void* b, const size_t size) {
    v_reserve(v, v->size + size);
    memcpy((uint8_t*)v->data + v->size, b, size);

#ifndef NDEBUG
    uint8_t *d = (uint8_t*)v->data + v->size;
    for(size_t i = 0; i < size; ++i)
        assert(d[i] == ((uint8_t*)b)[i]);
#endif

    v->size += size;
}

void* v_append(vector *v, const size_t size) {
    v_reserve(v, v->size + size);
    void *last = (uint8_t*)v->data + v->size;
    v->size += size;
    return last;
}


