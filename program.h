#ifndef PROGRAM_H
#define PROGRAM_H

#include "stream.h"
#include "vector.h"
#include "arch.h"

typedef struct program {
    uint8_t *data;
    size_t size, count;
} program;

program* program_create(dblock_t *d);
void program_destroy(program *p);

void dblock_map(dblock_t *d, int prot);
void dblock_unmap(dblock_t *d);
void dblock_protect(dblock_t *d, int prot);
size_t getPageSize();

void program_emit_b64(program *p, uint64_t s, size_t count);
void program_emit_d64(program *p, uint64_t s, size_t count);
void program_emit(program *p, uint8_t *src, size_t count);

#if defined(OS_WINDOWS)
#include <windows.h>
#error "Unimplemented"
#elif defined(OS_POSIX)

#define PROT_RW (PROT_READ | PROT_WRITE)
#define PROT_RE (PROT_READ | PROT_EXEC)
#endif

#endif
