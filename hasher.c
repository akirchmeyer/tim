#include "hasher.h"

uint32_t hash(const uint32_t *H, size_t n, const uint8_t *d) {
    uint32_t h = 0;
    while(n > 0) {
        h = (2*h + H[*d])%HASHER_MOD;
        ++d;
        --n;
    }

    return h;
}

uint32_t* hasher_create() {
    size_t n = 1 << (8*sizeof(uint8_t));
    uint32_t *H = malloc(sizeof(uint32_t) * n);
    uint32_t *h = H;
    while(n > 0) {
        *h = (uint32_t)(rand() % HASHER_MOD);
        ++h;
        --n;
    }

    return H;
}

void hasher_destroy(uint32_t *H) {
    free(H);
}
