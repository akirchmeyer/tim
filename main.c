#include <stdlib.h>
#include <time.h>

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>

#include "tests/test_stream.h"
#include "tests/test_vector.h"
#include "tests/test_parser.h"
#include "tests/test_program.h"
#include "tests/test_hashmap.h"
#include "tests/test_hash.h"

int main() {
    srand(time(0));

    //for(int i = 0; i < 256; ++i)
    //    printf("%c ", i);

    //Stream
    //read_file_test();
    //chunk_data_test();

    //Data Structures
    //vector_test();
    //test_hashmap();

    //Program

    test_parser();
//    test_program();
//    test_hash();

    return EXIT_SUCCESS;
}
