#include "hashmap.h"

#include <string.h>
#include <assert.h>
#include <stdio.h>

hashmap *hm_create(size_t s) {
    hashmap *h = malloc(sizeof(hashmap));
    hm_construct(h, s);
    return h;
}

void hm_destroy(hashmap *h) {
    hm_destruct(h);
    free(h);
}

void hm_construct(hashmap *h, size_t s) {
    h->size = s;

    const size_t c = s * sizeof(hm_bucket);
    h->buckets = malloc(c);
    hm_bucket* buckets = h->buckets;
    memset(buckets, 0, c);

    /*
    for(size_t i = 0; i < s; ++i) {
        buckets[i].size = 0;
        buckets[i].count = 0;
        buckets[i].data = NULL;
    }*/
}

void hm_destruct(hashmap *h) {
    hm_bucket *b = h->buckets;
    size_t n = h->size;
    while(n > 0) {
        if(b->count > 0)
            free(b->d);
        ++b;
        --n;
    }

    h->size = 0;
    free(h->buckets);
}

void hm_insert(hashmap *h, uint32_t k, void *v) {
    hm_bucket *b = &h->buckets[k%h->size];
    if(b->count == b->size) {
        b->count = (b->count == 0) ? A_COUNT : b->count << 1;
        b->d = realloc(b->d, b->count * sizeof(hm_node));
        assert(b->d != NULL);
    }

    hm_node *b_d = b->d + b->size;

    b_d->k = k;
    b_d->v = v;

    ++b->size;
}


void *hm_remove(hashmap *h, uint32_t k) {
    hm_bucket *b = &h->buckets[k%h->size];

    --b->size;
    size_t j = 0, m = b->size;
    hm_node *b_d = (hm_node*)b->d, *b_d2 = b_d + m;

    while(j <= m && b_d->k != k) {
        ++j;
        ++b_d;
    }
    if(j > m)
        return NULL;

    void *r = b_d->v;
    memcpy(b_d, b_d2, sizeof(hm_node));
    return r;
}

void* hm_remove_unsafe(hashmap *h, uint32_t k) {
    hm_bucket *b = &h->buckets[k%h->size];
    --b->size;
    hm_node *b_d = (hm_node*)b->d, *b_d2 = b_d + b->size;

    while(b_d->k != k)
        ++b_d;

    void *r = b_d->v;
    memcpy(b_d, b_d2, sizeof(hm_node));
    return r;
}

void *hm_get(hashmap *h, uint32_t k) {
    const hm_bucket *b = &h->buckets[k%h->size];
    hm_node *b_d = (hm_node*)b->d;

    size_t m = b->size;
    while(m > 0 && b_d->k != k) {
        --m;
        ++b_d;
    }
    if(m == 0)
        return NULL;

    return b_d->v;
}

void *hm_get_unsafe(hashmap *h, uint32_t k) {
    hm_node *b_d = (hm_node*)h->buckets[k%h->size].d;

    while(b_d->k != k)
        ++b_d;
    return b_d->v;
}

void hm_print(hashmap *h) {
    size_t n = h->size;
    hm_bucket *b = h->buckets;
    while(n > 0) {
        size_t m = b->size;
        hm_node *a = (hm_node*)b->d;

        while(m > 0) {
            printf("%u ", a->k);
            ++a;
            --m;
        }

        ++b;
        --n;
    }
}
