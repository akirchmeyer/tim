#include "arch.h"
#include "dlib.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef OS_POSIX

#include <dlfcn.h>

dlib* dlib_open(const char* name) {
    //TODO: Add option to change open mode
    void *handle = dlopen(name, RTLD_LAZY);
    if(!handle) {
        fprintf(stderr, "dlib: %s\n", dlerror());
        exit(EXIT_FAILURE);
    }
    dlerror();

    return handle;
}

void dlib_close(dlib *handle) {
    if(dlclose(handle) != 0)
        fprintf(stderr, "dlib: %s\n", dlerror());
    dlerror();
}

void *dlib_loadsymbol(dlib *handle, const char *symbol)
{
    void *addr = dlsym(handle, symbol);
    char *error = dlerror();
    if (error != NULL) {
        fprintf(stderr, "dlib: %s\n", error);
        exit(EXIT_FAILURE);
    }
    return addr;
}

#else
#error "Unimplemented"
#endif
