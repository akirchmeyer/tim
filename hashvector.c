#include "hashvector.h"

typedef struct hv_info {
    uint32_t k;
} hv_info;

void hv_construct(h_vector *h, size_t s) {
    v_construct(&h->k);
    v_construct(&h->v);
    v_push(&h->v, &(int) {
        0
    }, 1);
    hm_construct(&h->h, s);
}

void hv_destruct(h_vector *h) {
    hv_restore(h, 0);
    v_destruct(&h->k);
    v_destruct(&h->v);
    hm_destruct(&h->h);
}

void* hv_get(h_vector *h, uint32_t k) {
    size_t r = (size_t)hm_get(&h->h, k);
    if(r == 0)
        return NULL;
    return (void*)((uint8_t*)h->v.data + r);
}

void* hv_get_unsafe(h_vector *h, uint32_t k) {
    return (void*)((uint8_t*)h->v.data + (size_t)hm_get_unsafe(&h->h, k));
}

void hv_push_header(h_vector *h, uint32_t k) {
    hv_info i = {
        .k = k,
    };
    v_push(&h->k, &i, sizeof(hv_info));
    hm_insert(&h->h, k, (void*)(h->v.size));
}

size_t hv_save(h_vector *h) {
    return h->k.size;
}

void hv_restore(h_vector *h, size_t s) {
    hv_info *i = (hv_info*)((uint8_t*)h->k.data + s);
    size_t n = (h->k.size - s) / sizeof(hv_info);
    if(n == 0)
        return;

    while(n > 1) {
        hm_remove_unsafe(&h->h, i->k);
        ++i;
        --n;
    }
    size_t r = (size_t)hm_get_unsafe(&h->h, i->k);
    hm_remove_unsafe(&h->h, i->k);
    h->k.size = s;
    h->v.size = r;
}
