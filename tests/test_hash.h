#include "test.h"
#include "hasher.h"

void test_hash() {
    uint32_t *H1 = hasher_create();
    uint32_t* H2 = hasher_create();

    const size_t M = 1 << 15;

    const int32_t V = 1e9;
    const size_t s_str = 64;

    uint8_t **str = (uint8_t**)malloc(M * sizeof(uint8_t*));
    int32_t *val = (int32_t*)malloc(M * sizeof(int32_t));

    for(size_t i = 0; i < M; ++i) {
        const uint32_t r = rand() % UINT32_MAX;
        val[i] = (r % 2*V) - V;
        str[i] = (uint8_t*)malloc(s_str * sizeof(uint8_t));
        for(size_t j = 0; j < s_str; ++j)
            str[i][j] = (uint8_t)(r % UINT8_MAX);
    }

    for(size_t i = 0; i < M; ++i) {
        uint32_t v = hash(H1, s_str, str[i]);
        uint32_t w = hash(H2, s_str, str[i]);
        assert_int_equal(v != w, 1);
    }

    for(size_t i = 0; i < M; ++i)
        free(str[i]);
    free(str);
    free(val);

    hasher_destroy(H1);
    hasher_destroy(H2);
}
