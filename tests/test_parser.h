#include "test.h"
#include "hasher.h"
#include <stdio.h>
#include <string.h>

#include "stream.h"
#include "hashmap.h"
#include "mk.h"
#include "compiler.h"

typedef struct parser {
    vector *stack;
    compiler *com;
    int esym;
} parser;

parser *parser_create(uint8_t *data) {
    parser *p = malloc(sizeof(parser));
    p->stack = v_create();
    p->esym = 0;
    p->com = compiler_create(data);

    return p;
}

void parser_destroy(parser *p) {
    v_destroy(p->stack);
    compiler_destroy(p->com);
    free(p);
}

int state1(mk_machine *m);
int state2(mk_machine *m);

/*
 * - Push several processes
 * - Communicate between processes with public accessible data
 * - Use functions as delimiters
 */

/*
 * symbol
 * - priority
 */

/* LANGUAGE
 * struct
 * meta-language: #<function> <arguments> [<meta-arguments>]
    * ex: Synonyms: #define "." meta_get
    * #define <name of symbol> [<additional>] <expression>
    * #import <shared library> <name of symbol>
    * #push_process
    * #pop_process
    * #add_state <process id> <function id> <next>
    * #del_state <process id> <state id>
    * #return <number>
 * functions:
    * Define as symbol: #define "symbol" [d_argc=n] { code }
    * Call with certain number of arguments: <func>_<argc> <arg_1> <arg_...> <arg_n>
    * Or: (<func> <args>)
 * independent/parallelisable : []
 * array and pack, priority and code block: ()
 * Call functions
 */


/*
 * #set <type> <sym> <expr>
    * <type> = byte, char, int, uint, real, bignum, code
    * <type> is defined by interpreter: can add more
    * intermediate code will replce them with: s8, s16, s32, s64, s128, ...; u8, ...; r16, r32, r40, r64, r80, r128; bn; code
    * first read will transform interpreted types into fixed types
 * #unset <sym>
 * namespaces
 * #hal <sym> <count> = heap alloc
 * set <addr> <expr>
 * addr <sym> = take address of, pointer
 * deref <sym> = take value of, dereference
 * #get <sym/meta-sym>
 * */

#include <sys/mman.h>

#include "arch.h"
#ifdef OS_POSIX
#include <gnu/lib-names.h>
#else
#error "Unimplemented."
#endif
#include "program.h"
#include "dlib.h"


void test_parser() {
    uint8_t data[] =
        "#dbg_sym " \
        "#set v add 0.12345678901234567e17 sub mul 456.123 -789.987e-2 div 536 200 " \
        "{ printf \"Hello world!\\n\" } "
        // "{ printf \"%lu\\n\" 123 }"
        ;

    stream_t rs;
    rs.pFunc = read_data;

    dblock_t d;
    d.count = sizeof(data);
    d.data = data;
    rs.pData = &d;

    const size_t PAGE_SIZE = getPageSize();

    dblock_t d2;
    d2.count = 2*PAGE_SIZE;
    dblock_map(&d2, PROT_RW);

    parser* q = parser_create(d2.data);

    //Read file
    const size_t num = 2;
    mk_pFunc funcs[] = {state1, state2};
    size_t nexts[] = {0,0};

    mk_process *p = mkCreateProcess(&rs, NULL, funcs, nexts, num);

    compiler_link_std(q->com);
    mkProcessExecute(p, q);
    mkDestroyProcess(p);

    dblock_protect(&d2, PROT_RE);

    size_t count = d2.count;
    d2.count = q->com->size;
    print_hex(&d2);
    fflush(stdout);
    d2.count = count;

    void(*function)() = (void*)d2.data;
    function();
    dblock_unmap(&d2);

    parser_destroy(q);
}

#include <math.h>

int64_t readInt(size_t n, const uint8_t *data) {
    int64_t m = 0, sgn = 1;
    if(*data == '-') {
        sgn = -1;
        ++data;
    }
    uint8_t b;
    while(n > 0 && (b = *data-'0') <= 9) {
        m = 10*m + b;
        ++data;
        --n;
    }
    return sgn * m;
}

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KBLU  "\x1B[34m"

void pushLiteral(compiler *c, size_t n, const uint8_t *data) {
    int state = 0, sgn = 1, exp = 0;
    uint64_t man = 0;

    while(n > 0) {
        const uint8_t b = *data;

        switch(state) {
        case 0:
            state = 1;
            if(b == '-') {
                sgn = -1;
                break;
            }
            if(b < '0' || '9' < b) {
                if(b == '"') {
                    n -= 2;
                    ++data;
                }
                compiler_push_data(c, n, (uint8_t*)data);
                printf("%s\"%.*s\"%s ", KGRN, (int)n, data, KNRM);
                return;
            }
        case 1:
            if(b == '.') {
                exp = -(n-1);
                state = 2;
                break;
            }
        case 2:
            if(b == 'e' || b == 'E') {
                if(state == 2)
                    exp += n;
                exp += readInt(n-1, data+1);
                n = 1;
                break;
            }
            man = man*10 + b-'0';
            break;

        }
        ++data;
        --n;
    }
    if(exp >= 0) {
        int64_t b = sgn*man*(int64_t)pow(10, exp);
        printf("%s%ld%s ", KBLU, b, KNRM);
        compiler_push_i64(c, b);
        return;
    }
    else {
        double b = (double)man * pow(10.0, (double)exp);
        printf("%s%.9f%s ", KBLU, b, KNRM);
        compiler_push_d64(c, b);
        return;
    }
}

int state1(mk_machine *m) {
    const char b = m->byte;
    parser *q = m->data;

    if(b == '"') {
        q->esym = 1 - q->esym;
        return 0;
    }

    if(!(b == ' ' || b == '\0' || b == '\n') || q->esym)
        return 0;

    vector *v_stack = q->stack;

    if(v_stack->size == 0)
        return 1;

    const size_t v_size = v_stack->size;
    uint8_t *v_data = v_stack->data;
    compiler *c = q->com;

    if(hv_get(&c->sym, compiler_hash(c, v_size, v_data)) == NULL)
        pushLiteral(c, v_size, v_data);
    else {
        printf("%s%.*s%s ", KRED, (int)v_size, v_data, KNRM);
        compiler_push_call(c, compiler_hash(c, v_size, v_data));
    }

    //Clear stack
    memset(v_stack->data, 0, v_stack->size);
    v_stack->size = 0;

    return 1;
}

int state2(mk_machine *m) {
    parser *q = m->data;
    vector *v_stack = q->stack;
    v_push(v_stack, &m->byte, sizeof(uint8_t));

    return 1;
}


