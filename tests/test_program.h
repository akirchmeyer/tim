#include "test.h"
#include <stdio.h>
#include <string.h>
#include "program.h"
#include "dlib.h"

#include <sys/mman.h>

#ifdef OS_POSIX
#include <gnu/lib-names.h>
#else
#error "Unimplemented."
#endif

#include "arch.h"

void test_program() {

    const size_t PAGE_SIZE = getPageSize();
    const size_t p_size = PAGE_SIZE;

    dblock_t d;
    d.count  = p_size;
    dblock_map(&d, PROT_RW);

    program *p = program_create(&d);
    dlib *dlibc = dlib_open(LIBC_SO);

    void *d_printf = dlib_loadsymbol(dlibc, "printf");

    const char *data = "Hello World!\n";
    printf("%p %p\n", (void*)data, d_printf);

    //mov rdi, data
    program_emit_b64(p, 0x48BF, 2);
    program_emit_d64(p, (uint64_t)data, sizeof(uint64_t));
    //mov rdx, function2
    program_emit_b64(p, 0x48BA, 2);
    program_emit_d64(p, (uint64_t)d_printf, sizeof(uint64_t));

    program_emit_b64(p, 0xFFD2, 2); //call rdx
    program_emit_b64(p, 0x4831C0, 3); //xor rax, rax
    program_emit_b64(p, 0xC3, 1); //ret

    dblock_protect(&d, PROT_RE);

    //typedef void(*pFunc)();
    /*long (*function)(long, long) = (void*)d.data;
    assert_memory_equal(function, d.data, d.count);
    long a = 123456, b = 234567;
    printf("%ld %ld %ld\n", a, b, function(a,b));
    assert_int_equal(a+b, function(a, b));*/

    long (*function)() = (void*)d.data;
    assert_data_equal(function, d.data, p->size);

    size_t count = d.count;
    d.count = p->size;
    print_hex(&d);
    d.count = count;
    long res = function();
    printf("Return: %ld\n", res);

    dlib_close(dlibc);
    program_destroy(p);

    dblock_unmap(&d);
}
