#include <stdint.h>
#include <stdbool.h>

#include "vector.h"
#include "test.h"


void vector_test() {
    vector* v = v_create();

    int size = 1024;

    int a[size];
    for(int i = 0; i < size; ++i)
        a[i] = rand() % INT32_MAX;

    v_push(v, a, sizeof(a));
    int *data = v->data;

    for(int i = 0; i < size; ++i)
        assert_int_equal(data[i], a[i]);

    char b[size];
    for(int i = 0; i < size-1; ++i)
        b[i] = rand() % UINT8_MAX;
    b[size-1] = '\0';

    v_destroy(v);
    v = v_create();
    v_push(v, b, sizeof(b));

    char *data2 = (char*)((uint8_t*)v->data + sizeof(a));
    assert_string_equal(b, data2);

    v_destroy(v);
}

