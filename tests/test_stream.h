#include "stream.h"
#include "test.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

uint8_t* generate(const size_t size)
{
    uint8_t* data = malloc(size);

    for(size_t i = 0; i < size-1; ++i)
        data[i] = rand() % UINT8_MAX;
    data[size-1] = '\0';

    return data;
}

void read_file_test() {
    const char* FILENAME = "test";

    const size_t SIZE = 1024;
    uint8_t* data = generate(SIZE);
    {
        FILE *file = fopen(FILENAME, "wb");
        assert(file);

        fwrite(data, sizeof(uint8_t), SIZE, file);
        assert(!ferror(file));

        fclose(file);
    }
    {
        FILE *file = fopen(FILENAME, "rb");
        assert(file);

        //test past file
        uint8_t *dest = malloc(SIZE + 1000);
        dblock_t d;
        d.data = dest;
        d.count = SIZE + 1000;
        read_file(&d, file);

        assert_string_equal(data, dest);
        fclose(file);

        free(dest);
        dest = NULL;
    }


    free(data);
    data = NULL;

    //Deleting file
    assert(!remove(FILENAME));
}

void chunk_data_test() {
    const size_t SIZE = 10000;
    uint8_t* data = generate(SIZE);


    stream_t stream;
    stream.pFunc = read_data;

    dblock_t rd;
    rd.data = data;
    rd.count = SIZE;
    stream.pData = &rd;

    chunk_t chunk;
    chunk.pStream = &stream;
    chunk.size = 1024;

    {
        uint8_t *dest = malloc(SIZE);
        dblock_t wd;
        wd.data = dest;
        wd.count = SIZE;
        stream_chunk(&wd, &chunk);

        assert_string_equal(data, dest);
        free(dest);
        dest = NULL;
    }

    free(data);
    data = NULL;
}
