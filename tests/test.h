#ifndef TEST_H
#define TEST_H

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#define _ASSERT_ARGS __LINE__, __FILE__, __func__

void _error(size_t line, const char* file, const char* func, const char *msg) {
    fprintf(stderr, "Error in %s() at %s line %zu : %s", func, file, line, msg);
}
#define assert_terminate(msg) _error(_ASSERT_ARGS, msg)

void _assert_string_equal(size_t line, const char* file, const char* func, const char* msg, const char *a, const char *b) {
    int e = 1;
    while(*a != '\0' && e) {
        e = (*a == *b);
        ++a; ++b;
    }
    if(!e || *a != *b) {
        _error(line, file, func, msg);
        fprintf(stderr, "\"%s\" != \"%s\"\n", a, b);
        exit(-1);
    }
}
#define assert_string_equal(a,b) _assert_string_equal(_ASSERT_ARGS, "", (const char*)(a), (const char*)(b))

void _assert_null(size_t line, const char* file, const char* func, const char *msg, void *d) {
    if(d != NULL) {
        _error(line, file, func, msg);
        exit(-1);
    }
}
#define assert_null(d) _assert_null(_ASSERT_ARGS, "Data non-null\n", (void*)(d))

void _assert_non_null(size_t line, const char* file, const char* func, const char *msg, void *d) {
    if(d == NULL) {
        _error(line, file, func, msg);
        exit(-1);
    }
}
#define assert_non_null(d) _assert_non_null(_ASSERT_ARGS, "Data null\n", (void*)(d))

void _assert_data_equal(size_t line, const char* file, const char* func, const char *msg, const uint8_t *a, const uint8_t *b, size_t n) {
    int e = 1;
    while(n > 0 && e) {
        e = (*a == *b);
        ++a; ++b;
        --n;
    }
    if(!e) {
        _error(line, file, func, msg);
        fprintf(stderr,"\"%.*s\" != \"%.*s\"\n", (int)n, a, (int)n, b);
        exit(-1);
    }
}
#define assert_data_equal(a, b, n) _assert_data_equal(_ASSERT_ARGS, "", (const uint8_t*)(a), (const uint8_t*)(b), (size_t)(n))

void _assert_int_equal(size_t line, const char* file, const char* func, const char *msg, int a, int b) {
    if(a != b) {
        _error(line, file, func, msg);
        fprintf(stderr, "%d != %d\n", a, b);
        exit(-1);
    }
}

#define assert_int_equal(a, b) _assert_int_equal(_ASSERT_ARGS, "", (int)(a), (int)(b))

#endif
