#include "test.h"
#include "hasher.h"
#include "hashmap.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


void test_hashmap() {
    const size_t N = 128;
    hashmap *h = hm_create(N);

    const size_t M = 1 << 15;
    //printf("Allocating\n");
    //fflush(stdout);
    char **str = (char**)malloc(M * sizeof(char*));
    int32_t *val = (int32_t*)malloc(M * sizeof(int32_t));

    const size_t s_str = 64;
    const int32_t V = 1e9;
    for(size_t i = 0; i < M; ++i) {
        const uint32_t r = rand() % UINT32_MAX;
        val[i] = (r % 2*V) - V;
        str[i] = (char*)malloc(s_str * sizeof(char));
        for(size_t j = 0; j < s_str; ++j)
            str[i][j] = (char)(r % UINT8_MAX);
    }

    uint32_t *H = hasher_create();
    //printf("Inserting\n");
    //fflush(stdout);

    for(size_t i = 0; i < M; ++i) {
        uint32_t k = hash(H, s_str, (const uint8_t*)str[i]);
        hm_insert(h, k, &val[i]);
        hm_remove(h, k);
        hm_insert(h, k, &val[i]);
        hm_remove_unsafe(h, k);
        hm_insert(h, k, &val[i]);
    }

    /*hm_bucket *buckets = h->buckets;
    for(size_t i = 0; i < N; ++i) {
        if(buckets[i].size > M/N)
            printf("%d ", (int)buckets[i].size - (int)(M/N));
        //for(size_t j = 0; j < buckets[i].size; ++j)
        //    printf("> %s\n", ((hm_pair*)(buckets[i].data))[j].k);
    }
    printf("\n");*/

    //printf("Getting\n");
    //fflush(stdout);
    for(size_t i = 0; i < M; ++i) {
        uint32_t k = hash(H, s_str, (const uint8_t*)str[i]);
        int32_t *v = (int32_t*)hm_get(h, k);
        int32_t *w = (int32_t*)hm_get_unsafe(h, k);
        assert_non_null(v);
        int32_t m = *v;
        assert_int_equal(val[i], m);
        m = *w;
        assert_int_equal(val[i], m);
    }
    //printf("Destroying\n");
    //fflush(stdout);

    for(size_t i = 0; i < M; ++i)
        free(str[i]);
    free(str);
    free(val);

    hasher_destroy(H);

    hm_destroy(h);
}
