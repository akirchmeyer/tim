#ifndef DLIB_H
#define DLIB_H

typedef void dlib;

dlib *dlib_open(const char *name);
void* dlib_loadsymbol(dlib* handle, const char *symbol);
void dlib_close(dlib *handle);

#endif
