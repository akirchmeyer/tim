#ifndef POOL_H
#define POOL_H

#include <stdlib.h>
#include <stdint.h>

typedef void*(*pFuncAlloc)(size_t);
typedef void(*pFuncFree)(void*);

typedef struct pool {
    size_t b_size, r_blocks, offset;
    void *first, *cur;
    pFuncAlloc f_alloc;
    pFuncFree f_free;
} pool;

void p_construct(pool *p, size_t b_size, pFuncAlloc fa, pFuncFree ff) {
    p->b_size = b_size;
    p->r_blocks = 0;
    p->offset = sizeof(void*);

    void *ptr = fa(b_size);
    p->first = ptr;
    p->cur = ptr;

    p->f_alloc = fa;
    p->f_free = ff;
}

void p_destruct(pool *p) {
    size_t n = p->n_blocks;
    while(n > 0) {
        p->f_free(*blocks);
        ++blocks, --n;
    }
}

void p_alloc(void *ptr, size_t i, size_t n, size_t b, pFuncAlloc fa) {
    //go to last block
    while(i > 0) {
        ptr = (void*)*ptr;
        --i;
    }
    while(n > 0) {
        //allocate new block
        void *next = fa(b);
        //first element of block is pointer to the next one
        *ptr = next;
        ptr = next;
        --n;
    }
}

#include <string.h>

size_t min(size_t a, size_t b) {
    return (a < b) ? a : b;
}
void p_push(pool *p, uint8_t *d, size_t n) {
    size_t i2 = (p->offset + n + p->b_size - 1) / p->b_size;
    if(p->r_blocks <= i2) {
        p_alloc(p->cur, p->r_blocks, i2+1-p->r_blocks, p->b_size, p->f_alloc);
        p->r_blocks = 1;
    }
    else
        p->r_blocks -= i2;

    while(n > 0) {
        const size_t m = min(p->b_size - p->offset, n);
        memcpy(p->cur + p->offset, d, m);
        n -= m;
        d += m;
        p->offset += m;

        if(p->offset == p->b_size) {
            p->offset = sizeof(void*);
            p->cur = (void*)*p->cur;
        }
    }
}

#endif
