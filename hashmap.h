#ifndef HASHMAP_H
#define HASHMAP_H

#include <stdlib.h>
#include <stdint.h>

typedef struct __attribute__ ((__packed__)) hm_node {
    uint32_t k;
    void *v;
} hm_node;

typedef struct hm_bucket {
    hm_node *d;
    size_t size, count;
} hm_bucket;

typedef struct hashmap {
    hm_bucket* buckets;
    size_t size;
} hashmap;

#define A_COUNT 16

hashmap *hm_create(size_t s);
void hm_destroy(hashmap *h);

void hm_construct(hashmap *h, size_t s);
void hm_destruct(hashmap *h);

void hm_insert(hashmap *h, uint32_t k, void *v);
void *hm_remove(hashmap *h, uint32_t k);
void *hm_remove_unsafe(hashmap *h, uint32_t k);
void *hm_get(hashmap *h, uint32_t k);
void *hm_get_unsafe(hashmap *h, uint32_t k);

void hm_print(hashmap *h);

#endif
